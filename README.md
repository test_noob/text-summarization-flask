# Text Summarizer

**Technologies Used**: Python, Flask, Wikipedia API, NLTK, MongoDB 

## Purpose and Reason

There is so much information now on the internet and on sites such as Wikipedia. However many sites including 
Wikipedia overwhelms you with loads of information, pictures and links. I find that many times, when I am on 
these sights, I am mostly looking for a general overview of the topic that I am searching. I bulit text-summarizer
to do just that.

Here is an implementation of two different algorithms to summarize the text. The first uses simple maths. It calculates the count of each
word and stores them as values. Each sentence is then given a total value score and if a sentence scores higher than the 
weight * average score, it gets concatenated to the final summary string. This is a very trivial solution.The second algorithm is the well known TF-IDF algorithm which is short for term frequency–inverse document frequency. Finally, I have created low-level, mid-level, and high-level summarization options which simply change the value of the weight.

## Getting Started

### Prerequisites

You will need Python 3.8 to install Flask, Wikipedia API, NLTK, and PyMongo


### Install

```bash
pip install flask nltk wikipedia pymongo
```

### Explaining TF IDF

An information retrieval, tf–idf or TFIDF, short for term frequency–inverse document frequency, is a numerical statistic that is intended to reflect how important a word is to a document in a collection or corpus. It is often used as a weighting factor in searches of information retrieval, text mining, and user modeling. The tf–idf value increases proportionally to the number of times a word appears in the document and is offset by the number of documents in the corpus that contain the word, which helps to adjust for the fact that some words appear more frequently in general. tf–idf is one of the most popular term-weighting schemes today. A survey conducted in 2015 showed that 83% of text-based recommender systems in digital libraries use tf–idf.

Variations of the tf–idf weighting scheme are often used by search engines as a central tool in scoring and ranking a document's relevance given a user query. tf–idf can be successfully used for stop-words filtering in various subject fields, including text summarization and classification.

One of the simplest ranking functions is computed by summing the tf–idf for each query term; many more sophisticated ranking functions are variants of this simple model.

TF-IDF algorithm is made of 2 algorithms multiplied together (Term Frequency * Inverse Document Frequency)

TF(t) = (Number of times word w appears in a document) / (Total number of words in the document)

IDF(t) = log_e(Total number of documents / Number of documents with term t in it)
